-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2018 a las 12:15:53
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `contenido` longtext COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `titulo`, `contenido`, `autor`, `fecha`) VALUES
(1, 'Estos son los barrios ''ricos'' en los que Zaragoza duplica el IBI a los garajes comunitarios', 'Se calcula que en Zaragoza hay unos 200 párquines comunitarios que se han visto afectados por el cambio normativo.\r\n<br><br>\r\n Varios garajes de la calle de José Oto, en el barrio de La Jota, entre los afectados\r\nVarios garajes de la calle de José Oto, en el barrio de La Jota, entre los afectadosGoogle Maps\r\nCon la recurrente justificación de que la medida afecta exclusivamente a inmuebles  propiedad de personas o familias de rentas altas, el gobierno de ZEC –con el apoyo  de PSOE y CHA, que ahora quieren rectificar- ha aplicado este año una subida del IBI a los garajes comunitarios con escritura colectiva y un valor catastral de más de un millón de euros que ha duplicado el importe a  miles de vecinos. \r\n<br><br>\r\nComo informó este lunes Heraldo.es el cambio afecta  los aparcamientos que  se inscribieron en el registro con una escritura colectiva y tienen un valor conjunto de más de un millón de euros. En este supuesto, la Gerencia del Catastro -dependiente del Ministerio de Hacienda-  cataloga dichos inmuebles como almacén de propiedad única. Se calcula que en Zaragoza hay unos 200 párquines comunitarios como estos, con miles de plazas en su conjunto, y todo ellos se han visto afectados  por el cambio normativo.', 'heraldo', '2018-05-29 00:00:00'),
(2, 'La peluquería con más juego de Zaragoza', 'La peluquería barbería de Daniel Vicente, en la calle de Conde de la Viñaza, 16, ubicada en el corazón del barrio de las Delicias, abrió sus puertas hace tres años. No es un establecimiento más y tampoco pasa inadvertido. Recientemente, su propietario ha decidido decorarlo con varios elementos relacionados con una de sus mayores aficiones: los videojuegos de los años 90.\r\n\r\n“Colecciono videojuegos retro desde que era pequeño porque mi hermano tenía un ordenador MSX, de aquellos que cargaban los juegos con cinta y con cartucho. De ahí me viene todo”, cuenta Daniel Vicente.\r\n\r\n \r\nLa Feria del Videojuego Primera Pantalla se llena de jugadores\r\nLeer más\r\nAl principio, abrió “con lo justo”, aunque reconoce que siempre tuvo en mente decorarla con temática de videojuegos de la década de los 90, principalmente (‘Tetris’, ‘Super Mario Land’, ‘Street Fighter II’, ‘Virtual Striker II’, ‘Time Crisis’...). “En España, no creo que haya muchas barberías de este estilo -comenta-, así que decidí juntar mis dos aficiones: el coleccionismo de videojuegos con mi otra pasión, la peluquería”.', 'heraldo', '2018-05-29 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcionCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagenCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`, `descripcionCat`, `imagenCat`) VALUES
(2, 'Pelicula Real', NULL, NULL),
(3, 'Cine de autor', 'Peliculas de cine de autor', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `idImg` int(11) NOT NULL,
  `descripcionImg` text COLLATE utf8_spanish2_ci,
  `archivoImg` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idProd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImg`, `descripcionImg`, `archivoImg`, `idProd`) VALUES
(3, NULL, '1526985480101.jpg', 38),
(4, NULL, '1526985480101.jpg', 38),
(5, NULL, '1526985480101.jpg', 38),
(10, NULL, '1527082722_101.jpg', 41),
(11, NULL, '1527077336_102.jpg', 41),
(12, NULL, '1527079409_301.jpg', 41),
(13, NULL, '1527082658_schindler_s_list-473662617-large.jpg', 3),
(14, NULL, '1527081755_vengadores.jpg', 4),
(15, NULL, '1527081355_vengadores 2.jpg', 5),
(16, NULL, '1527250247_ant_man-627211491-large.jpg', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPago` int(11) NOT NULL,
  `nombrePago` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPedido` int(11) NOT NULL,
  `fechaPedido` date NOT NULL,
  `cantidadPedido` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProd` int(11) NOT NULL,
  `nombreProd` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcionProd` text COLLATE utf8_spanish2_ci,
  `precioProd` double(5,2) NOT NULL,
  `unidadesProd` int(11) NOT NULL,
  `fechaAlta` date NOT NULL,
  `activado` bit(1) NOT NULL,
  `idCat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProd`, `nombreProd`, `descripcionProd`, `precioProd`, `unidadesProd`, `fechaAlta`, `activado`, `idCat`) VALUES
(3, 'La lista de Schildler 2', 'descripcion de la peli', 15.00, 7, '2018-05-19', b'1', 2),
(4, 'vengadores 3', 'Peliculas de los vengadores', 150.00, 8, '2018-05-17', b'1', 2),
(5, 'vengadores 2', 'pelicula de los vengadores', 56.00, 3, '2018-05-14', b'1', 2),
(6, 'ant-man', 'pelicula de micro hormigas', 5.00, 3, '2018-05-15', b'1', 2),
(7, 'lo que el viento se llevo', 'pelicula de clark ', 6.00, 87, '2018-05-30', b'1', 1),
(8, 'y si no nos enfadamos', 'pelicula de bud spencer y terence hill', 60.00, 8, '2018-05-18', b'1', 1),
(9, 'El codigo Da vinci', 'Pelicula sobre una trama policia de tal y cual....', 6.00, 7, '2018-05-18', b'1', 0),
(10, 'Le llamaban trinidad', 'Gran pelicula del oeste', 7.00, 3, '2018-05-18', b'1', 0),
(11, 'Venganza', 'Pelicula de Accion ambientada en un pais de moda', 8.00, 2, '2018-05-18', b'1', 0),
(12, 'El cortador de cesped 2', 'Pelicula sobre realizad virtual', 6.00, 2, '2018-05-18', b'1', 0),
(13, 'matrix', 'Pelicula sobre mundos de realidad virtual, pionera en su genero', 32.00, 21, '2018-05-18', b'1', 0),
(14, 'El rey leon', 'Pelicula del año 1995 que cuenta la historia de ....', 15.00, 2, '2018-05-18', b'1', 0),
(15, 'Curso 1999', 'Unos profesores, se cargan a sus alumnos porque si', 12.00, 45, '2018-05-18', b'1', 0),
(16, 'Depredador', 'Pelicula sobre CHOCHENAGUER', 45.00, 2, '2018-05-18', b'0', 0),
(17, 'Terminator', 'Pelicula sobre un cyborg del futuro, con desnudos incorporados', 23.00, 63, '2018-05-19', b'1', 0),
(18, 'Terminator 2', 'Mas terminator que nunca', 45.00, 3, '2018-05-19', b'1', 0),
(19, 'La habitacion de panico', 'Habitacion de seguridad en una mansion, donde hay un atraco, y nadie muere', 15.00, 21, '2018-05-19', b'1', 0),
(20, 'Venganza 2', 'Mas venganza que la anterior, pero menos que la siguiente. Aqui muere hasta el apuntador.', 5.00, 6, '2018-05-19', b'1', 0),
(21, 'Taxi', 'Pelicula de un taxista fitipaldi, que le gusta la velocidad mas que a un tonto un caramelo', 45.00, 3, '2018-05-19', b'1', 0),
(27, 'El ataque de los tomates ', 'Descripcion de la pelicula', 15.00, 2, '2018-05-15', b'1', 0),
(37, 'aaa', 'aaa', 111.00, 111, '2018-05-22', b'1', 0),
(38, '66', '66', 66.00, 66, '2018-05-22', b'1', 0),
(41, 'aaa', 'aaa', 222.00, 333, '2018-05-23', b'1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `nombreProveedor` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoProveedor` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `nombreProveedor`, `telefonoProveedor`) VALUES
(1, '23123', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `login` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `session` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `nombre`, `apellidos`, `login`, `password`, `correo`, `avatar`, `session`) VALUES
(1, 'david', 'fraj blesa', 'admin', 'ee10c315eba2c75b403ea99136f5b48d', 'admin@gmail.com', NULL, '4037a7a9c09c49d9532c09a2fca52f9f'),
(2, 'sergio', 'fraj blesa', 'sergiofraj', '81dc9bdb52d04dc20036dbd8313ed055', 'sergio@gmail.com', NULL, ''),
(3, 'esther', 'apellidos', 'esther', '81dc9bdb52d04dc20036dbd8313ed055', 'esther@gmail.com', NULL, ''),
(4, 'david', 'apellidos', 'david', '81dc9bdb52d04dc20036dbd8313ed055', 'david@gmail.com', NULL, ''),
(5, 'thor', 'illa', 'thor', '81dc9bdb52d04dc20036dbd8313ed055', 'thor@gmail.com', NULL, ''),
(6, 'asdasdas', 'asdasdasd', 'asdasdasd', '81dc9bdb52d04dc20036dbd8313ed055', 'david@gmail.com', NULL, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`idImg`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPedido`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProd`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `idImg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
