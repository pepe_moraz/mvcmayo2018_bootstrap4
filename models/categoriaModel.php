<?php  
//Fichero models/categoriaModel.php

class Categoria{
	public $id;
	public $nombre;
	public $descripcion;

	public function __construct($registro){
		$this->id=$registro['idCat'];
		$this->nombre=$registro['nombreCat'];
		$this->descripcion=$registro['descripcionCat'];
	}
} //Fin de la class Post
?>